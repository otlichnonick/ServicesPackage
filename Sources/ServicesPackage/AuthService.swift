//
//  AuthService.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation

public protocol IAuthService {
    func login<Request: Codable, Response: Codable>(input: Request, handler: @escaping (Result<Response, Error>) -> Void)
    func register<Request: Codable, Response: Codable>(input: Request, handler: @escaping (Result<Response, Error>) -> Void)
    func logout<Response: Codable>(handler: @escaping (Result<Response, Error>) -> Void )
}

public class AuthService: IAuthService {
    static let shared: IAuthService = AuthService.init()
    
    public init() {}
    
    public func login<Request, Response>(input: Request, handler: @escaping (Result<Response, Error>) -> Void) where Request : Decodable, Request : Encodable, Response : Decodable, Response : Encodable {

    }
    
    public func register<Request, Response>(input: Request, handler: @escaping (Result<Response, Error>) -> Void) where Request : Decodable, Request : Encodable, Response : Decodable, Response : Encodable {
        
    }
    
    public func logout<Response>(handler: @escaping (Result<Response, Error>) -> Void) where Response : Decodable, Response : Encodable {
        
    }
}
