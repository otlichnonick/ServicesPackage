//
//  SettingsService.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation
import SwiftUI

public enum ColorThemes {
    case regular
    case blackAndWhite
    case sepia
    
    public var textColor: UIColor {
        switch self {
        case .regular:
            return .white
        case .blackAndWhite:
            return .black
        case .sepia:
            return .brown
        }
    }
    
    public var textBackgroundColor: UIColor {
        switch self {
        case .regular:
            return .gray
        case .blackAndWhite:
            return .white
        case .sepia:
            return UIColor(red: 255/255, green: 235/255, blue: 229/255, alpha: 1)
        }
    }
}

public enum FontSizes {
    case regular
    case small
    case big
}

public protocol ISettingsService {
    var theme: ColorThemes { get }
    var fontSize: FontSizes { get }
    
    func changeTheme(with newTheme: ColorThemes)
    func changeFontSize(with newFontSize: FontSizes)
}

public class SettingsService: ISettingsService {
    static let shared: ISettingsService = SettingsService.init()

    private(set) public var theme: ColorThemes = .regular
    private(set) public var fontSize: FontSizes = .regular
    
    public init() {}
    
    public func changeTheme(with newTheme: ColorThemes) {
        self.theme = newTheme
    }
    
    public func changeFontSize(with newFontSize: FontSizes) {
        self.fontSize = newFontSize
    }
}
