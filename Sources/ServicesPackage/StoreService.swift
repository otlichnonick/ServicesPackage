//
//  StoreService.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation

public protocol IStoreService {
    func create<T>(newObject: T)
    func read<T>() -> T?
    func put<T>(object: T)
    func delete()
}

public class StoreService: IStoreService {
    static let shared: IStoreService = StoreService.init()
    
    public init() {}
    
    public func create<T>(newObject: T) {
        
    }
    
    public func read<T>() -> T? {
        return nil
    }
    
    public func put<T>(object: T) {
        
    }
    
    public func delete() {
        
    }
    
    
}
