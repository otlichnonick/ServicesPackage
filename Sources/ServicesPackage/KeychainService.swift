//
//  KeychainService.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation

public protocol IKeychainService {
    func getAccessToken() -> String?
    func getRefreshToken() -> String?
    func set(accessToken newValue: String)
    func set(refreshToken newValue: String)
}

public class KeychainService: IKeychainService {
    static let shared: IKeychainService = KeychainService.init()
    
    private var accessToken: String?
    private var refreshToken: String?
    
    public init() {}
    
    public func getAccessToken() -> String? {
        return accessToken
    }
    
    public func getRefreshToken() -> String? {
        return refreshToken
    }
    
    public func set(accessToken newValue: String) {
        accessToken = newValue
    }
    
    public func set(refreshToken newValue: String) {
        refreshToken = newValue
    }
}
